# Etiqueta de imagen base específica (ubi9.5)
FROM registry.access.redhat.com/ubi9:9.3-1476 AS build

# Instalación de Git, tar y gzip
RUN yum install -y git tar gzip

# Instalación de oc, kubectl y argocd CLI
RUN yum install -y wget
RUN wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux.tar.gz && \
    tar -xvf openshift-client-linux.tar.gz -C /usr/local/bin oc kubectl && \
    wget https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 -O /usr/local/bin/argocd && \
    chmod +x /usr/local/bin/argocd

# Imagen final UBI Micro
FROM registry.access.redhat.com/ubi9-micro:9.3-9

# Copiar Git, oc, kubectl y argocd desde la imagen temporal
COPY --from=build /usr/bin/git /usr/bin/git
COPY --from=build /usr/lib64/ /usr/lib64/
COPY --from=build /usr/local/bin/ /usr/local/bin/


# Definir el usuario no-root para la imagen
USER 1001
