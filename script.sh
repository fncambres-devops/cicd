#!/bin/bash

TOKEN=12345
PRIVATE_TOKEN=123456
PROJECT_ID=${CI_PROJECT_ID}

curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" --request POST --form "variable_name=TOKEN" --form "variable_value=$TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/variables"
